import controller.*;
import org.junit.*;
import static org.junit.Assert.*;
public class CalcTest {
    private static Polinom polinom1;
    private static Polinom polinom2;
    private static int nrTests = 0;
    private static int nrTestsPassed = 0;

    @BeforeClass
    public static void initializare() {
        System.out.println("Initializare Calculator!");
        polinom1 = new Operatii();
        polinom2 = new Operatii();
    }

    @AfterClass
    public static void finalizare() {
        polinom1 = null;
        polinom2 = null;
        System.out.println(nrTestsPassed + "/" + nrTests);
    }

    @Before
    public void inc() {
        nrTests++;
        polinom1 = new Operatii();
        polinom2 = new Operatii();
    }

    @After
    public void testTerminat() {
        System.out.println("Test Terminat!");
    }

    @Test
    public void adunare() {
        String pol1 = "4x^2+4.0x+1.0";
        String pol2 = "-2.0x^2+2.0x-1.0";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.adunareScadere(polinom2, 1);
            assertEquals("2.0x^2+6.0x", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void adunare2() {
        String pol1 = "2x^3+5x^2-3x+2";
        String pol2 = "-x^2+4x-1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.adunareScadere(polinom2, 1);
            assertEquals("2.0x^3+4.0x^2+x+1.0", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void scadere() {
        String pol1 = "x^2+4.0x+1.0";
        String pol2 = "2x^2+2.0x-1.0";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom2.adunareScadere(polinom1, 0);
            assertEquals("-x^2+2.0x+2.0", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void scadere2() {
        String pol1 = "2x^3+5x^2-3x+2";
        String pol2 = "-x^2+4x-1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom2.adunareScadere(polinom1, 0);
            assertEquals("2.0x^3+6.0x^2-7.0x+3.0", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void inmultireTest() {
        String pol1 = "-x-1";
        String pol2 = "x+1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.inmultire(polinom2);
            assertEquals("-x^2-2.0x-1.0", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void inmultireTest2() {
        String pol1 = "3x^2+2x+1";
        String pol2 = "2x-1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.inmultire(polinom2);
            assertEquals("6.0x^3+x^2-1.0", rezultat.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void impartireTest() {
        String pol1 = "6.0x^5-17.0x^3-x^2+3";
        String pol2 = "3.0x^2-6.0x+2";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom[] rezultat = polinom1.impartire(polinom2);
            assertEquals("2.0x^3+4.0x^2+x-1.0", rezultat[0].toString());
            assertEquals("-8.0x+5.0", rezultat[1].toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void derivareTest() {
        String pol1 = "x^2+2.0x+1";
        String pol2 = "x+1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.derivare();
            Polinom rezultat1 = polinom2.derivare();
            assertEquals("2.0x+2.0", rezultat.toString());
            assertEquals("1.0", rezultat1.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }

    @Test
    public void integrareTest() {
        String pol1 = "3.0x^2+2.0x+1";
        String pol2 = "2.0x+1";
        try {
            polinom1 = Parse.prelucrareInput(pol1);
            polinom2 = Parse.prelucrareInput(pol2);
            Polinom rezultat = polinom1.integrare();
            Polinom rezultat1 = polinom2.integrare();
            assertEquals("x^3+x^2+x", rezultat.toString());
            assertEquals("x^2+x", rezultat1.toString());
            nrTestsPassed++;
        } catch (Exception e) {
            assertEquals(0, 1);
            System.out.println("Invalid Input!");
        }
    }
}