package repository;

import controller.*;

public interface InterfataOperatii {

    public Polinom adunareScadere(Polinom polinom, Integer flag);

    public void init(Polinom polinom, Integer flag);

    public Polinom inmultire(Polinom polinom);

    public Polinom[] impartire(Polinom impartitor);

    public Polinom derivare();

    public Polinom integrare();

    public Integer gradMax();

    public Polinom inmultireConst(Integer grad, Double coeficient);

    public void eliminare();
}