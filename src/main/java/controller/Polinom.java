package controller;

import java.util.*;
import repository.InterfataOperatii;
public abstract class Polinom implements InterfataOperatii {
    private Map<Integer, Number> elements;

    public Polinom() {
        elements = new HashMap<>();
    }

    public void adaugaElement(Integer grad, Double coeficient) {
        elements.put(grad, coeficient);
    }

    public String toString() {
        this.eliminare();
        if (this.elements.isEmpty()) {
            return "0";
        }
        List<String> rezultat = new ArrayList<>();
        for (Map.Entry<Integer, Number> indice : elements.entrySet()) {
            Integer grad = indice.getKey();
            Double coeficient = (Double) indice.getValue();
            String s = (coeficient >= 0) ? "+" : "";
            if (grad.equals(0)) {
                rezultat.add(s + coeficient);
            } else if (grad.equals(1)) {
                rezultat.add(s + coeficient + "x");
            } else {
                rezultat.add(s + coeficient + "x^" + grad);
            }
        }
        Collections.reverse(rezultat);
        for (String indice : rezultat)
            if (indice.contains("x"))
                rezultat.set(rezultat.indexOf(indice), indice.replace("1.0", ""));
        rezultat.set(0, rezultat.get(0).replace("+", ""));
        return String.join("", rezultat);
    }

    public Map<Integer, Number> getElements() {
        return elements;
    }

    public void setElements(Map<Integer, Number> elements) {
        this.elements = elements;
    }
}