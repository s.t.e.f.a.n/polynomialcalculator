package controller;
import java.util.*;
import java.util.regex.*;
public class Parse {
    public static Polinom prelucrareInput(String inputPol) throws Exception {
        Polinom polinom = new Operatii();
        inputPol = inputPol.replace("-", "+-");
        inputPol = inputPol.replace(" ", "");
        String[] sir = inputPol.split("\\+");
        for (String indice : sir) {
            if (!indice.isEmpty()) {
                String grad = null;
                String coeficient = null;
                if (indice.contains("^"))
                    grad = indice.substring(indice.indexOf("^") + 1);
                else {
                    if (indice.contains("x"))
                        grad = "1";
                    else grad = "0";
                }
                if (indice.contains("x")) {
                    coeficient = indice.substring(0, indice.indexOf("x"));
                    if (coeficient.isEmpty()) coeficient = "1.0";
                    else if (coeficient.equals("-")) coeficient = "-1.0";
                } else coeficient = indice;
                Integer grad1 = Integer.parseInt(grad);
                Double coeficient1 = Double.parseDouble(coeficient);
                polinom.adaugaElement(grad1, coeficient1);
            }
        }
        return polinom;
    }
}