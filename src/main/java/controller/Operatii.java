package controller;

import java.util.*;

public class Operatii extends Polinom {

    public void init(Polinom polinom, Integer flag) {
        for (Map.Entry<Integer, Number> indice : getElements().entrySet()) {
            polinom.adaugaElement(indice.getKey(), flag * (2 * (Double) indice.getValue()) - (Double) indice.getValue());
        }
    }

    public Polinom adunareScadere(Polinom polinom, Integer flag) {// In functie de flag se face adunare sau scadere
        Polinom rezultat = new Operatii();
        init(rezultat, flag);
        for (Map.Entry<Integer, Number> indice : polinom.getElements().entrySet()) {
            Integer grad = indice.getKey();
            Double coeficient = (Double) indice.getValue();
            if (rezultat.getElements().containsKey(grad)) {
                coeficient += (Double) rezultat.getElements().get(grad);
            }
            rezultat.adaugaElement(grad, coeficient);
        }
        return rezultat;
    }

    public Polinom inmultire(Polinom polinom) {
        Polinom rezultat = new Operatii();
        for (Map.Entry<Integer, Number> indice1 : this.getElements().entrySet()) {
            for (Map.Entry<Integer, Number> indice2 : polinom.getElements().entrySet()) {
                Integer grad1 = indice1.getKey();
                Integer grad2 = indice2.getKey();
                Double coeficient1 = (Double) indice1.getValue();
                Double coeficient2 = (Double) indice2.getValue();
                Integer gNou = grad1 + grad2;
                Double cNou = coeficient1 * coeficient2;
                if (rezultat.getElements().containsKey(gNou)) {
                    cNou += (Double) rezultat.getElements().get(gNou);
                }
                rezultat.adaugaElement(gNou, cNou);
            }
        }
        return rezultat;
    }

    public Integer gradMax() {
        if (!this.getElements().isEmpty())
            return Collections.max(getElements().keySet());
        return 0;
    }

    public Polinom inmultireConst(Integer grad, Double coeficient) {
        Polinom rezultat = new Operatii();
        rezultat.adaugaElement(grad, coeficient);
        return this.inmultire(rezultat);
    }

    public void eliminare() {
        List<Integer> el = new ArrayList<>();
        for (Map.Entry<Integer, Number> indice : this.getElements().entrySet()) {
            if (Math.abs((Double) indice.getValue()) < 0.0001)
                el.add(indice.getKey());
        }
        for (Integer indice : el)
            this.getElements().remove(indice);
    }

    public Polinom[] impartire(Polinom impartitor) {
        Polinom rezultat = new Operatii();
        Polinom rest = new Operatii();
        Polinom temporar = new Operatii();

        while (this.gradMax() >= impartitor.gradMax() && !this.getElements().isEmpty()) {
            Integer grad = this.gradMax() - impartitor.gradMax();
            Double coeficient = (Double) this.getElements().get(this.gradMax()) / (Double) impartitor.getElements().get(impartitor.gradMax());
            rezultat.adaugaElement(grad, coeficient);
            temporar = impartitor.inmultireConst(grad, coeficient);
            this.setElements((temporar.adunareScadere(this, 0)).getElements());
            this.eliminare();
        }
        rest = this;
        return new Polinom[]{rezultat, rest};
    }

    public Polinom derivare() {
        Polinom rezultat = new Operatii();
        for (Map.Entry<Integer, Number> indice : this.getElements().entrySet()) {
            Integer grad = indice.getKey();
            Double coeficient = (Double) indice.getValue();
            if (grad >= 1) {
                rezultat.adaugaElement(grad - 1, coeficient * grad);
            }
        }
        return rezultat;
    }

    public Polinom integrare() {
        Polinom rezultat = new Operatii();
        for (Map.Entry<Integer, Number> indice : this.getElements().entrySet()) {
            Integer grad = indice.getKey();
            Double coeficient = (Double) indice.getValue();
            rezultat.adaugaElement(grad + 1, coeficient / (grad + 1));
        }
        return rezultat;
    }
}