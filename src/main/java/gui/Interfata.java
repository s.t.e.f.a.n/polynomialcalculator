package gui;

import controller.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interfata {
    public static void main(String[] args) {
        JFrame fr = new JFrame("Polynomial Calculator");
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setSize(500, 250);

        JPanel p = new JPanel();
        JPanel panelSt = new JPanel();
        JPanel panelDr = new JPanel();

        panelSt.setLayout(new BoxLayout(panelSt, BoxLayout.Y_AXIS));
        panelDr.setLayout(new BoxLayout(panelDr, BoxLayout.Y_AXIS));

        p.add(panelSt);
        p.add(Box.createRigidArea(new Dimension(20, 0)));
        p.add(panelDr);

        JLabel firstPol = new JLabel("Polynomial 1: ", JLabel.RIGHT);
        panelSt.add(firstPol);
        JTextField text1 = new JTextField("Enter the first polynomial here!", 22);
        panelDr.add(text1);

        Interfata.distance(panelSt, 25, panelDr, 20);

        JComboBox operation = new JComboBox(new String[]{"+", "-", "*", "/", "Derivare⬆", "Derivare⬇", "Integrare⇧", "Integrare⇩"});
        panelSt.add(operation);

        Interfata.distance(panelSt, 25, panelDr, 50);

        JLabel secondPol = new JLabel("Polynomial 2:", JLabel.RIGHT);
        panelSt.add(secondPol);
        JTextField text2 = new JTextField("Enter the second polynomial here!", 22);
        panelDr.add(text2);

        Interfata.distance(panelSt, 25, panelDr, 25);

        JButton egal = new JButton("=");
        panelSt.add(egal);
        JTextField resultText = new JTextField("Result.", 20);
        panelDr.add(resultText);

        egal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String operationSel = (String) operation.getSelectedItem();
                String t1 = text1.getText();
                String t2 = text2.getText();
                Polinom pol1 = new Operatii();
                Polinom pol2 = new Operatii();
                try {
                    pol1 = Parse.prelucrareInput(t1);
                    pol2 = Parse.prelucrareInput(t2);
                    if (operationSel.equals("+")) {
                        Polinom rezultat = pol1.adunareScadere(pol2, 1);
                        resultText.setText(rezultat.toString());
                    } else if (operationSel.equals("-")) {
                        Polinom rezultat = pol2.adunareScadere(pol1, 0);
                        resultText.setText(rezultat.toString());
                    } else if (operationSel.equals("*")) {
                        Polinom rezultat = pol1.inmultire(pol2);
                        resultText.setText(rezultat.toString());
                    } else if (operationSel.equals("Derivare⬆")) {
                        Polinom rezultat = pol1.derivare();
                        resultText.setText(rezultat.toString());
                    } else if (operationSel.equals("Derivare⬇")) {
                        Polinom rezultat = pol2.derivare();
                        resultText.setText(rezultat.toString());
                    } else if (operationSel.equals("Integrare⇧")) {
                        Polinom rezultat = pol1.integrare();
                        resultText.setText(rezultat.toString() + "+C");
                    } else if (operationSel.equals("Integrare⇩")) {
                        Polinom rezultat = pol2.integrare();
                        resultText.setText(rezultat.toString() + "+C");
                    } else if (operationSel.equals("/")) {
                        if (pol1.gradMax() < pol2.gradMax())
                            resultText.setText("0, Rest: " + pol1.toString());
                        else if (pol2.toString().equals("0") && !pol1.toString().equals("0"))
                            resultText.setText("Division by 0!");
                        else {
                            Polinom[] rezultat = pol1.impartire(pol2);
                            if (rezultat[0].toString().equals("0") && rezultat[1].toString().equals("0")) {
                                resultText.setText("Null!");
                            } else if (!rezultat[0].toString().equals("0") && rezultat[1].toString().equals("0")) {
                                resultText.setText(rezultat[0].toString());
                            } else if (!rezultat[0].toString().equals("0") && !rezultat[1].toString().equals("0")) {
                                resultText.setText(rezultat[0].toString() + ", Rest: " + rezultat[1].toString());
                            }
                        }
                    }
                } catch (Exception ex) {
                    resultText.setText("Invalid Input! Please Type Again!");
                }
            }
        });

        fr.setContentPane(p);
        fr.setVisible(true);
        fr.setResizable(false);
    }

    public static void distance(JPanel panelSt, Integer x, JPanel panelDr, Integer y) {
        panelSt.add(Box.createRigidArea(new Dimension(0, x)));
        panelDr.add(Box.createRigidArea(new Dimension(0, y)));
    }
}